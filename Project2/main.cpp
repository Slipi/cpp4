#include <iostream>
#include <exception>
class Fraction {
public:
	Fraction(int n, int d) {
		if (d == 0) throw std::runtime_error("Denominator = zero");
	}
private:
	int numerator;
	int denominator;
};
int main() {
	int a, b;
	std::cin >> a >> b;
	try {
		Fraction Frac = Fraction(a, b);
	}
	catch (std::exception& exception) {
		std::cout << exception.what();
	}
	return 0;
}